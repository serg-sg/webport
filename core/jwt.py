#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import secrets

def genaratorSecretsToken():
    return secrets.token_hex(4096)

if __name__ == '__main__':
    Token = genaratorSecretsToken()
    print(Token)
    #return Token