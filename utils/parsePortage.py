#-*- codding:UTF-8 -*-
#!/usr/bin/env python
import os, sys, json
from utils import load_config
class PortageConfig():
    def __init__(self):
        self.Config = load_config()
        self.TargetParse =['USE', 'USE_EXPAN', 'VIDEO_CARDS', 'ACCEPT_CHOSTS', 'ACCEPT_LICENSE','CCACHE_DIR', 'CCACHE_SIZE',
                    'EMERGE_DEFAULT_OPTS', 'CFLAGS', 'CXXFLAGS', 'MAKEOPTS']
        self.MakeConfig={}
    def getLines(self, filename):
        if os.path.exists(filename):
            with open (filename, 'r') as f:
                lines = f.read().split("\n")
        else:
            print("path is not Found")
        return lines

    def parseMakeConf(self):
        StateBox = 'Null'
        Lines = self.getLines(self.Config['Portage']['make.conf'])

        for line in Lines:
            if not line.startswith('#') and len(line.split("="))>1:
                line.replace('"', "")
                if StateBox == 'Null' and not line.endswith('"'):
                    StateBox = line.split('=')[0]
                    self.MakeConfig[StateBox] =[] 
                                    
                    self.MakeConfig[StateBox] = line.split('=')[1].split(" ")
                    for i in  line.split('=')[1].split(" "):
                        self.MakeConfig[split('=')[0]].append(i)
                elif StateBox !='Null' and not line.endswith('"'):
                    StateBox = line.split('=')[1]
                    for i in  line.split('=')[1].split(" "):
                        self.MakeConfig[line.split('=')[0]].append(i)
                elif StateBox == 'Null' and line.endswith('"'):
                    #line.replace('"', "")
                    self.MakeConfig[line.split('=')[0]] = line.split('="')[1].split(" ")
                    
        print(self.MakeConfig)

    def parsePackageUse(self):
        self.MakeConfig['package.use'] =[]
        Lines = self.getLines(self.Config['Portage']['package.use'])
        for i in Lines:
            self.MakeConfig['package.use'].append(dict(Name=i.split(" ")[0], Uses=i.split(" ")[1:]))

    def parsePackageMasks(self):
        Lines = self.getLines(self.Config['Portage']['package.mask'])

    def parsePakageUnmask(self):
        Lines = self.getLines(self.Config['Portage']["package.unmask"])

    def parsePackageKeywords(self):
        Lines = self.getLines(self.Config['Portage']['package.accept_keywords'])

if __name__ ==  '__main__':
    Test = PortageConfig()
    if sys.argv[1] == "envMake":   
        Test.parseMakeConf()
    elif sys.argv[1] == "use":
        Test.parsePackageUse()
    else:
        Test.parseMakeConf()
