#-*- codding: UTF-8 -*-
#!/usr/bin/env python
import os, sys

def get_global_USE():
    ListUSE = {}
    path = '/usr/portage/profiles/use.desc'
    with open(path) as f:
        lines = f.read().split("\n")
        for line in lines:
            if line.startswith('#'):
                pass
            else:
                try:
                    ListUSE[line.split(' -')[0]] = str(line)
                except Exception (e):
                    print(str(e))
    return ListUSE


def get_local_USE():
    ListUSE = {}
    path = '/usr/portage/profiles/use.local.desc'
    with open(path) as f:
        lines = f.read().split("\n")
        for line in lines:
            if line.startswith('#'):
                pass
            else:
                try:
                    ListUSE[line.split(':')[1].split(' -')[0]] = str(line.replace(line.split(':')[0] + ":", ""))
                except Exception as e:
                    print(str(e))
    print(ListUSE)
    return ListUSE
