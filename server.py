#!/usr/bin/env pyton3
# -*- coding: UTF-8 -*-
'__autor__' =='serkus'
from http.server import HTTPServer, SimpleHTTPRequestHandler, BaseHTTPRequestHandler
import logging
from core.handler import Handler as Handler
import os, sys, json, ssl
from core.route import Router
#PORT_NUMBER = 8000
from utils.findfsdb import create_db
from utils.utils import load_config

def https_server():
    https_server_address  = ('', 4443)
    httpd = HTTPServer(https_server_address, SimpleHTTPRequestHandler)
    httpd.socket = ssl.wrap_socket(httpd.socket,
                                    server_side = True,
                                    certfile = 'localhost.pem',
                                    ssl_version = ssl.PROTOCOL_TLS)
    httpd.serve_forever()

def run():
    create_db()
    config = load_config()
    if len(config) == 0:
        print("Файл конфигурации пустой.")
        return 1
    try:
        print(config['PORT'])
    except KeyError:
        print("KeyError")
    except KeyboardInterrupt:
        pass
    
    # server = HTTPServer(('', config['PORT']), Router)
    server = HTTPServer(('', config['PORT']), Handler)
    # print('Started HTTP Server on port', config['PORT'])
    
    server.socket = ssl.wrap_socket(server.socket,
                                    server_side = True,
                                    certfile = 'localhost.pem',
                                    ssl_version = ssl.PROTOCOL_TLS)
    print('Started HTTP Server on port', config['PORT'])
    print("https://localhost:8000")
    
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        logging.info('Stopping httpd...\n')
    server.server_close()
   
if __name__ == '__main__':
    #https_server()
    run()
    https_server()

