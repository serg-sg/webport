# Webport GRUSS ALLE

## О ПРОЕКТЕ

Это проект WebGUI для пакетного менеджера Portage (Gentoo/Calculate-linux).
Этот продукт представляет из себя клиент-серверное приложение.

## ЦЕЛЬ ПРОЕКТА

Предоставть удобный GUI-интерфейс для Portage, для таких утройств как планшеты, смартфоны, предоставить удалённый доступ к Portage на клиентской/удалённой машине, снизить порог вхождения для новых пользователей на дистрибутивах имеющих в своей основе пакетный менеджер Portage.

## КАК СОБРАТЬ И ЗАПУСТИТЬ

Нужен дистрибутив с пакетным менеджером Portage.
Скачайте дистрибутив серверного приложения проекта.

1. Введите в терминале:
`git clone https://git.calculate-linux.org/serkus/webport.git && cd webport`
2. Запустите:
`make run`
3. Скачайте клиент и запустите:

* Откройте клиент (если вы он у вас собран).
* Откройте в браузере страницу с адресом localhost:8080 или с IP-адресом и портом указанным в файле config.json (если вы установили как WebClient).
